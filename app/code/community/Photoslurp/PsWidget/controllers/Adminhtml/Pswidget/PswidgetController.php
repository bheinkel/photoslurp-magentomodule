<?php

class Photoslurp_PsWidget_Adminhtml_Pswidget_PswidgetController extends Mage_Adminhtml_Controller_Action
{
        protected function _initAction()
        {
                $this->loadLayout()->_setActiveMenu("pswidget/pswidget")
                    ->_addBreadcrumb(
                        Mage::helper("adminhtml")->__("Pswidget  Manager"),
                        Mage::helper("adminhtml")->__("Pswidget Manager")
                    );
                return $this;
        }

        protected function _isAllowed()
        {
            return Mage::getSingleton('admin/session')->isAllowed('pswidget/pswidget');
        }

        public function indexAction() 
        {
                $this->_title($this->__("Photoslurp Widget"));
                $this->_title($this->__("Manager Pswidget"));

                $this->_initAction();
                $this->renderLayout();
        }
        public function editAction()
        {                
                $this->_title($this->__("Photoslurp Widget"));
                $this->_title($this->__("Pswidget"));
                $this->_title($this->__("Edit Item"));
                
                $id = $this->getRequest()->getParam("id");
                $model = Mage::getModel("pswidget/pswidget")->load($id);

                if ($model->getId()) {
                    Mage::register("pswidget_data", $model);

                    $this->loadLayout();
                    $this->_setActiveMenu("pswidget/pswidget");
                    $this->_addBreadcrumb(
                        Mage::helper("adminhtml")
                        ->__("Photoslurp Widget Manager"), Mage::helper("adminhtml")->__("Pswidget Manager")
                    );
                    $this->_addBreadcrumb(
                        Mage::helper("adminhtml")
                        ->__("Pswidget Description"), Mage::helper("adminhtml")->__("Pswidget Description")
                    );
                    $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
                    $this->_addContent(
                        $this->getLayout()
                        ->createBlock("pswidget/adminhtml_pswidget_edit")
                    )
                        ->_addLeft($this->getLayout()->createBlock("pswidget/adminhtml_pswidget_edit_tabs"));
                    $this->renderLayout();
                } else {
                    Mage::getSingleton("adminhtml/session")
                        ->addError(Mage::helper("pswidget")->__("Item does not exist."));
                    $this->_redirect("*/*/");
                }
        }

        public function newAction()
        {

        $this->_title($this->__("PsWidget"));
        $this->_title($this->__("Pswidget"));
        $this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
        $model  = Mage::getModel("pswidget/pswidget")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("pswidget_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("pswidget/pswidget");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(
            Mage::helper("adminhtml")
            ->__("Photoslurp Widget Manager"), Mage::helper("adminhtml")->__("Pswidget Manager")
        );
        $this->_addBreadcrumb(
            Mage::helper("adminhtml")
            ->__("Photoslurp Widget Description"), Mage::helper("adminhtml")->__("Pswidget Description")
        );


        $this->_addContent($this->getLayout()->createBlock("pswidget/adminhtml_pswidget_edit"))
            ->_addLeft($this->getLayout()->createBlock("pswidget/adminhtml_pswidget_edit_tabs"));

        $this->renderLayout();

        }

        public function saveAction()
        {

            $postData=$this->getRequest()->getPost();

                if ($postData) {
                    try {
                        $widget = Mage::getModel("pswidget/pswidget")->load($this->getRequest()->getParam("id"));
                        $params = array_map(
                            function () {
                            return null; 
                            }, $widget->getData()
                        );
                        unset($params['id']);
                        $postData = array_merge($params, $postData);
                        if(isset($postData['lookbook_product_types'])){
                            $postData['lookbook_product_types'] = json_encode($postData['lookbook_product_types']);
                        }

                        if(isset($postData['category'])){
                            $postData['category'] = json_encode($postData['category']);
                        }

                        if(isset($postData['website'])){
                            $postData['website'] = json_encode($postData['website']);
                        }

                        $model = Mage::getModel("pswidget/pswidget")
                            ->setData($postData)
                        ->setId($this->getRequest()->getParam("id"))
                        ->save();

                        $io = new Varien_Io_File();
                        $io->setAllowCreateFolders(true);
                        $path = Mage::getBaseDir('media').'/photoslurp/';
                        $file = $path . $postData['widget_id'] . '.css';

                        if (isset($postData['css'])) {
                            $io->open(array('path' => $path));
                            $io->streamOpen($file, 'w+');
                            $io->streamLock(true);
                            $io->streamWrite($postData['css']);
                            $io->streamUnlock();
                            $io->streamClose();
                        } else {
                            $io->rm($file);
                        }

                        Mage::getSingleton("adminhtml/session")
                            ->addSuccess(Mage::helper("adminhtml")->__("Pswidget was successfully saved"));
                        Mage::getSingleton("adminhtml/session")
                            ->setPswidgetData(false);

                        if ($this->getRequest()->getParam("back")) {
                            $this->_redirect("*/*/edit", array("id" => $model->getId()));
                            return;
                        }

                        $this->_redirect("*/*/");
                        return;
                    } 
                    catch (Exception $e) {
                        Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                        Mage::getSingleton("adminhtml/session")->setPswidgetData($this->getRequest()->getPost());
                        $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                    return;
                    }
                }

                $this->_redirect("*/*/");
        }



        public function deleteAction()
        {
                if ($this->getRequest()->getParam("id") > 0) {
                    try {
                        $model = Mage::getModel("pswidget/pswidget");
                        $model->setId($this->getRequest()->getParam("id"))->delete();
                        Mage::getSingleton("adminhtml/session")
                            ->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                        $this->_redirect("*/*/");
                    } 
                    catch (Exception $e) {
                        Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                        $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                    }
                }

                $this->_redirect("*/*/");
        }

        
        public function massRemoveAction()
        {
            try {
                $ids = $this->getRequest()->getPost('ids', array());
                foreach ($ids as $id) {
                      $model = Mage::getModel("pswidget/pswidget");
                      $model->setId($id)->delete();
                }

                Mage::getSingleton("adminhtml/session")
                    ->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
            }
            catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
            }

            $this->_redirect('*/*/');
        }

}
