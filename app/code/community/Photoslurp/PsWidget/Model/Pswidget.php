<?php

class Photoslurp_PsWidget_Model_Pswidget extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {

       $this->_init("pswidget/pswidget");

    }

    public function getProductWidget($position)
    {
        return $this->getCollection()->addProductWidgetFilter($position);
    }

    public function getCategoryWidget($position)
    {
        return $this->getCollection()->addCategoryWidgetFilter($position);
    }

    public function getWidget($id)
    {
        return $this->getCollection()->addWidgetFilter($id);
    }

}
     