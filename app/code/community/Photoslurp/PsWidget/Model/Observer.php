<?php
class Photoslurp_PsWidget_Model_Observer {

    public function sendWidgetConverted(Varien_Event_Observer $observer) {
        if(Mage::getStoreConfig('tracking/tracking_configuration/tracking_enable')) {
            $order = Mage::getModel('sales/order')->load($observer->getEvent()->getOrder()->getId());
            $items = $order->getAllVisibleItems();
            $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
            $campaignId = Mage::getStoreConfig('tracking/tracking_configuration/campaign_id');

            $data = [
                'event' => 'widget_converted',
                'visitor_id' => $this->getVisitorId(),
                'order_id' => $order->getIncrementId(),
                'referrer' => Mage::getBaseUrl()
            ];
            if($campaignId){
                $data['album_id'] = $campaignId;
            }

            foreach ($items as $item){
                $data['products'][$item->getProduct()->getSku()] = array('count'=> $item->getQtyOrdered(),'price' => $item->getPrice(),'currency' => $currencyCode);
            }

            $uri = 'http://api.photoslurp.com/v3/widgets/record/';
            $client = new Zend_Http_Client($uri);
            try {
                $response = $client->setRawData(json_encode($data), 'application/json')->request('POST');
                $this->log('Response: '. $response);
            }
            catch(Zend_Http_Client_Exception $e) {
                unset($e);
            }
        }
    }

    protected function getVisitorId(){
        $this->log('Getting "ps_analytics" from cookies.');
        $visitorId = Mage::getModel('core/cookie')->get('ps_analytics');
        if(!$visitorId){
            $this->log('"ps_analytics" cookie does not exist. Generating "visitor_id"...');
            $visitorIdLength = 20;
            $visitorId = '';
            $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            for ($i=0; $i<$visitorIdLength; $i++){
                $visitorId .= $alphabet[random_int(0, 300)%strlen($alphabet)];
            }
        }
        $this->log('"visitor_id" : '.$visitorId);
        return $visitorId;
    }

    protected function log($message){
        if(Mage::getStoreConfig('tracking/tracking_configuration/log_enable')) {
            Mage::log($message, null, "photoslurp_debug.log");
        }
    }
}
