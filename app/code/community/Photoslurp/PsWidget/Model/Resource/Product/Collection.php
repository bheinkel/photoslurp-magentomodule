<?php
class Photoslurp_PsWidget_Model_Resource_Product_Collection extends Mage_Catalog_Model_Resource_Product_Collection
{
    public function addStoreIds()
    {
        $this->getSelect()->columns(array('store_ids' => "GROUP_CONCAT(store.store_id)"));
        return $this;
    }

    public function joinStores()
    {
        $this->getSelect()->joinInner(
            array('store'=> Mage::getSingleton('core/resource')
                ->getTableName('core/store')), "product_website.website_id=store.website_id"
        );
        return $this;
    }

    public function joinStoreAttribute($name, $type, $storeId)
    {
        $attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product', $name);
        $this->getSelect()->joinLeft(
            array('at_'.$name.'_'.$storeId =>
                Mage::getSingleton('core/resource')->getTableName('catalog_product_entity_'.$type)),
            '(`at_'.$name.'_'.$storeId.'`.`entity_id` = `e`.`entity_id`) AND (`at_'.$name.'_'.$storeId.
            '`.`attribute_id` = '.$attributeId.') AND `at_'.$name.'_'.$storeId.'`.`store_id` = '.$storeId,
            array($name.'_'.$storeId => 'IF(at_'.$name.'_'.$storeId
                .'.value_id > 0, at_'.$name.'_'.$storeId.'.value, at_'.$name.'_default.value)')
        );
    }

    public function addProductStoreData($stores)
    {
        $magentoVersion = Mage::getVersion();
        if (version_compare($magentoVersion, '1.13', '>=')){
            $urlAttrName = 'url_key';
            $urlAttrType = 'url_key';
        }
        else {
            $urlAttrName = 'url_path';
            $urlAttrType = 'varchar';
        }

        $this->joinDefaults('name', 'varchar');
        $this->joinDefaults('description', 'text');
        $this->joinDefaults($urlAttrName, $urlAttrType);

        foreach ($stores as $store) {
            $storeId = $store->getId();
            $this->joinStoreAttribute('name', 'varchar', $storeId);
            $this->joinStoreAttribute('description', 'text', $storeId);
            $this->joinStoreAttribute($urlAttrName, $urlAttrType, $storeId);
        }
    }

    protected function joinDefaults($name, $type)
    {
        $attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product', $name);
        $this->getSelect()->joinLeft(
            array('at_'.$name.'_default' => Mage::getSingleton('core/resource')
                ->getTableName('catalog_product_entity_'.$type)),
            '(`at_'.$name.'_default`.`entity_id` = `e`.`entity_id`) AND (`at_'.$name.'_default`.`attribute_id` = '
            .$attributeId.') AND `at_'.$name.'_default`.`store_id` = 0',
            array('at_'.$name.'_default' => 'value')
        );
    }
}