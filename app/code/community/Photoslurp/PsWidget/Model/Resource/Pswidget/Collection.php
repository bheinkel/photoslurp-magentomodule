<?php
    class Photoslurp_PsWidget_Model_Resource_Pswidget_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

        public function _construct()
        {
            $this->_init("pswidget/pswidget");
        }

        public function addProductWidgetFilter($position)
        {
            if(count(Mage::app()->getWebsites())>1){
                $this->addFilter('website',array('like' => '%"'.Mage::app()->getWebsite()->getId().'"%'), 'public');
            }
            return $this -> addFilter('widget_enable', '1')
                    -> addFilter('page_type', '2')
                    -> addFilter('position', $position)
                    -> setPageSize(1)
                    -> getFirstItem();
        }

        public function addCategoryWidgetFilter($position)
        {
            if(count(Mage::app()->getWebsites())>1){
                $this->addFilter('website',array('like' => '%"'.Mage::app()->getWebsite()->getId().'"%'), 'public');
            }
            $category = Mage::registry('current_category');
            $this->addFieldToFilter(
                'category',
                array(
                    array('like' => '%"'.$category->getId().'"%'),
                    array('null' => true)
                )
            );
            return $this -> addFilter('widget_enable', '1')
                -> addFilter('page_type', '4')
                -> addFilter('position_category', $position)
                -> setPageSize(1)
                -> getFirstItem();
        }

        public function addWidgetFilter($id)
        {
            return $this -> addFilter('id', $id)
                -> addFilter('widget_enable', '1')
                -> setPageSize(1)
                -> getFirstItem();
        }

        

    }
     