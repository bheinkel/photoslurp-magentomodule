<?php
class Photoslurp_PsWidget_Model_Resource_Pswidget extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init("pswidget/pswidget", "id");
    }
}