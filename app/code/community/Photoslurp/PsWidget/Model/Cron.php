<?php
class Photoslurp_PsWidget_Model_Cron
{

    protected $_delimiter = '|';

    protected $_productHelper = null;

    protected $_helper = null;

    protected $_exportableStores = array();

    protected $_categoryName = array();

    protected $_categoryPath = array();

    public function __construct()
    {
        $this->_productHelper = Mage::helper('catalog/product');
        $this->_helper = Mage::helper('pswidget');

        if (Mage::getStoreConfig('export/export_configuration/store_id')) {
            $this->_exportableStores = explode(",", Mage::getStoreConfig('export/export_configuration/store_id'));
        }

    }

    public function photoslurpExport()
    {
//cron
        if (Mage::getStoreConfig('export/export_configuration/cron_enable')) {
            $this->export(Mage::getStoreConfig('export/export_configuration/export_type'));
        }
    }

    public function manualExport($productType)
    {
        $this->export($productType);
    }

    protected function isRatesSpecified()
    {

        $baseCurrencyCode = Mage::app()->getBaseCurrencyCode();

        $allowedCurrencies = Mage::getModel('directory/currency')
            ->getConfigAllowCurrencies();

        $currencyRates = Mage::getModel('directory/currency')
            ->getCurrencyRates($baseCurrencyCode, array_values($allowedCurrencies));

        foreach ($allowedCurrencies as $currency) {
            if (!isset($currencyRates[$currency])) {
                return false;
            }
        }

        return true;
    }

    protected function export($productType)
    {
        $stores = $this->getExportStores();
        foreach ($stores as $store) {
            $categoryCollection = Mage::getResourceModel('catalog/category_collection')->addNameToResult();
            $categoryCollection->setStoreId($store->getId());
            foreach ($categoryCollection as $category){
                $this->_categoryName[$store->getId()][$category->getId()] = $category->getName();
                $this->_categoryPath[$store->getId()][$category->getId()] = $category->getPath();
            }
            foreach ($this->_categoryPath[$store->getId()] as $id => $path){
                $pathArray = explode('/',$path);
                unset($pathArray[0]);
                $this->_categoryPath[$store->getId()][$id] = implode('>',
                    array_intersect_key($this->_categoryName[$store->getId()], array_flip($pathArray)));
            }
        }


        if (!$this->isRatesSpecified()) {
            Mage::throwException(
                Mage::helper('adminhtml')->__('Currency rate is not specified (System -> Manage Currency -> Rates)')
            );
        }

        $io = new Varien_Io_File();
        $io->setAllowCreateFolders(true);
        $path = $this->_helper->getExportPath();
        $file = $this->_helper->getExportFile();
        $io->open(array('path' => $path));
        $io->streamOpen($file, 'w+');
        $io->streamLock(true);


        $extVersion = $this->_helper->getExtensionVersion();
        $date = Mage::getModel('core/date')->gmtDate('d-m-Y');
        $mageVersion = Mage::getVersion();
        $io->streamWrite("# exported at $date ver. $extVersion $mageVersion". PHP_EOL);

        $websites = $this->getExportWebsites();

        $collection = Mage::getResourceModel('pswidget/product_collection')
            ->joinTable(
                'cataloginventory/stock_item',
                'product_id=entity_id',
                array('stock_status' => 'is_in_stock')
            )
            ->addAttributeToSelect(array('image','price'), 'left')
            ->addAttributeToFilter('visibility', array("neq"=>1))
            ->addAttributeToFilter('status', 1)
            ->addWebsiteFilter($websites);

        if(Mage::getResourceModel('catalog/eav_attribute')->loadByCode('catalog_product','gender')->getId()){
            $collection->addAttributeToSelect(array('gender'), 'left');
        }

        $collection->addStoreIds();

        $collection->joinStores();


        $collection->getSelect()->group('e.entity_id');

        if ($productType == 'simple') {
            $collection ->addAttributeToFilter(
                'type_id',
                array('eq' => Mage_Catalog_Model_Product_Type::TYPE_SIMPLE)
            );
        }

        if ($productType == 'parent') {
            $collection ->addAttributeToFilter(
                'type_id',
                array('neq' => Mage_Catalog_Model_Product_Type::TYPE_SIMPLE)
            );
        }

        $collection->addProductStoreData($this->getExportStores());

        $header = array("sku");
        $currencyCodes = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
        foreach ($currencyCodes as $code) {
            $header[] = 'price_'.$code;
        }

        $header[]='in_stock';
        $header[]='image_url';
        $stores = $this->getExportStores();
        foreach ($stores as $store) {
            $languageCode = Mage::getStoreConfig('general/locale/code', $store->getId()).'_'.$store->getId();
            $header[] = 'title_'.$languageCode;
            $header[] = 'description_'.$languageCode;
            $header[] = 'url_'.$languageCode;
            $header[] = 'product_types_'.$languageCode;
        }

        $header[]='google_category';
        $header[]='gender';

        $io->streamWrite(implode($header, $this->_delimiter) . PHP_EOL);

        Mage::getSingleton('core/resource_iterator')->walk(
            $collection->getSelect(),
            array(array($this, 'productCallback')),
            array(
                'io' => $io,
                'stores'=> $stores,
                'currency_codes' => $currencyCodes,
                'base_currency_code' => Mage::app()->getBaseCurrencyCode())
        );

        $io->streamUnlock();
        $io->streamClose();
    }

    protected function getExportWebsites()
    {
        $stores = $this->getExportStores();
        $websites = array();
        foreach ($stores as $store) {
            $websites[] = $store->getWebsiteId();
        }

        $websites = array_unique($websites);
        return $websites;
    }

    protected function exportableStores($store)
    {
        return in_array($store->getId(), $this->_exportableStores);
    }

    protected function getExportStores()
    {
        $stores = Mage::app()->getStores();
        if ($this->_exportableStores) {
            $stores = array_filter($stores, array($this, 'exportableStores'));
        }

        foreach ($stores as $store) {
            $languageCode = Mage::getStoreConfig('general/locale/code', $store->getId());
            $store->setLangCode($languageCode);
        }

        return $stores;
    }

    public function productCallback($args)
    {

        $product = Mage::getModel('catalog/product');
        $product->setData($args['row']);

        $insertData = array();

        $insertData['sku'] = $args['row']['sku'];

        $price = $args['row']['price'];
        foreach ($args['currency_codes'] as $code) {
            $insertData['price_'.$code] = $this->convertPrice($price, $args['base_currency_code'], $code);
        }

        $insertData['in_stock'] = ($args['row']['stock_status'])?'in stock':'out of stock';

        if ($product->getImage() && ($product->getImage() != 'no_selection')) {
            $insertData['image_url'] = Mage::getModel('catalog/product_media_config')
                ->getMediaUrl($product->getImage());
        } else {
            $insertData['image_url'] = '';
        }

        $stores = $args['stores'];
        $productStores = explode(',', $product->getStoreIds());

        $pattern = '/"/';
        $replacement = '$0"';

        foreach ($stores as $store) {
            $langcode = $store->getLangCode().'_'.$store->getId();
            $isProductInStore = in_array($store->getId(), $productStores);
            $insertData['title_'.$langcode]         =  $isProductInStore ? sprintf(
                '"%s"',
                preg_replace(
                    $pattern,
                    $replacement,
                    $args['row']['name_'.$store->getId()]
                )
            ) : '';
            $insertData['description_'.$langcode]   =  $isProductInStore ? sprintf(
                '"%s"',
                preg_replace($pattern, $replacement, $args['row']['description_'.$store->getId()])
            ) : '';

            $magentoVersion = Mage::getVersion();
            if (version_compare($magentoVersion, '1.13', '>=')){
                $productUrlSuffix = $this->_productHelper->getProductUrlSuffix($store->getId());
                if ($productUrlSuffix && (strpos($productUrlSuffix, '.') !== 0))
                    $productUrlSuffix = '.'.$productUrlSuffix;
                $insertData['url_'.$langcode]           =  $isProductInStore ? $store->getBaseUrl()
                    . $args['row']['url_key_'.$store->getId()] .$productUrlSuffix. '?___store=' . $store->getCode() : '';
            }
            else {
                $insertData['url_'.$langcode]           =  $isProductInStore ? $store->getBaseUrl()
                    . $args['row']['url_path_'.$store->getId()] . '?___store=' . $store->getCode() : '';
            }
            $categoryPaths = array();
            $categoryIds = $product->getCategoryIds();
            if($categoryIds){
                foreach ($categoryIds as $id){
                    $categoryPaths[$store->getId()][] = $this->_categoryPath[$store->getId()][$id];
                }
                $insertData['product_types_'.$langcode] = implode(',',$categoryPaths[$store->getId()]);
            }else{
                $insertData['product_types_'.$langcode] = "";
            }
        }

        $insertData['google_category'] = '';
        $insertData['gender'] = $product->getGender();

        $args['io']->streamWrite(implode($insertData, $this->_delimiter) . PHP_EOL);
    }

    protected function convertPrice($price,$from,$to)
    {
        return Mage::helper('directory')->currencyConvert($price, $from, $to);
    }
}