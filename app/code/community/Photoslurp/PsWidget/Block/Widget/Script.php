<?php
class Photoslurp_PsWidget_Block_Widget_Script extends Mage_Core_Block_Template
{

    protected $_widget = null;

    protected $_product = null;

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('pswidget/widget/script.phtml');
        $this->setCacheLifetime(false);
    }

    public function getCacheKeyInfo()
    {
        $cacheKeyInfo = array_merge(parent::getCacheKeyInfo(), array($this->getId(),$this->getNameInLayout()));
        if($this->getRequest()->getControllerName() == 'category'){
            $category = Mage::registry('current_category');
            $cacheKeyInfo = array_merge($cacheKeyInfo, array($category->getId()));
        }
        return $cacheKeyInfo;
    }

    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = Mage::registry('product');
        }

        return $this->_product;
    }

    public function getWidget()
    {
        if (!$this->_widget) {
            if ($this->getData('page_type') == 'product') {
                $this->_widget = Mage::getModel('pswidget/pswidget')->getProductWidget($this->getPosition());
            } elseif($this->getData('page_type') == 'category') {
                $this->_widget = Mage::getModel('pswidget/pswidget')->getCategoryWidget($this->getPosition());
            } else {
                $this->_widget = Mage::getModel('pswidget/pswidget')->getWidget($this->getData('id'));
            }
        }

        return $this->_widget;
    }

    public function getWidgetData()
    {
        $localeCode = Mage::app()->getLocale()->getLocaleCode();
        $storeId = Mage::app()->getStore()->getStoreId();
        $data = $this->getWidget()->getData();
        $data['lang'] = $localeCode.'_'.$storeId;
        $widgetData = array_merge($data);
        if ($additionalParams = json_decode('{'.$this->getWidget()->getAdditionalParams().'}', true)) {
            $widgetData = array_merge($widgetData, $additionalParams);
        }

        return $widgetData;
    }

    public function getJson()
    {
        $data = $this->getWidgetData();
        $product = $this->getProduct();
        if ($product) {
            $skus = array();
            $skus[] = $product->getSku();
            if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
                $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $product);
                foreach ($childProducts as $child) {
                    $skus[] =  $child->getSku();
                }
            }
            $data['product_id'] = $skus;
        }

        $pageTypes = Photoslurp_PsWidget_Block_Adminhtml_Pswidget_Grid::getValueArray10();
        $data['page_type']   = $pageTypes[$data['page_type']]['label'];

        if (isset($data['lookbook_product_types'])) {
            $types = json_decode($data['lookbook_product_types']);
            $lbCategories = Mage::getResourceModel('catalog/category_collection')->addNameToResult()
                ->addAttributeToFilter('entity_id',$types);
            $lbProductTypes = array();
            foreach ($lbCategories as $lbCategory){
                $lbProductTypes[] = Mage::helper('pswidget')->getCategoryPath($lbCategory);
            }
            $data['product_type'] = $lbProductTypes;
        }

        unset($data['id']);
        unset($data['widget_enable']);
        unset($data['position']);
        unset($data['position_category']);
        unset($data['user_name']);
        unset($data['id_widget']);
        unset($data['id_lang']);
        unset($data['css']);
        unset($data['page_type']);
        unset($data['website']);
        unset($data['lookbook_product_types']);
        if($data['product_type'] == 0) unset($data['product_type']);
        unset($data['category']);
        unset($data['display_in_categories']);
        unset($data['widget_id']);

        $parameters = array();
        foreach ($data as $path=>$value) {
            if (($value !== null)) {
                if (strpos($path, 'style_')=== 0) {
                    $temp = &$parameters;
                    foreach (explode('_', $path) as $key) {
                        $temp = &$temp[$key];
                    }

                    $temp = $value;
                    unset($temp);
                } else {
                    $jsName = preg_replace_callback(
                        '/_([^_])/',
                        function (array $m) {
                            return ucfirst($m[1]);
                        },
                        $path
                    );
                    $parameters[$jsName] = $value;
                }
            }
        }

        return json_encode($parameters);
    }
}