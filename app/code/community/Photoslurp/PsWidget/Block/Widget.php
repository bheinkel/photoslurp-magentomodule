<?php
class Photoslurp_PsWidget_Block_Widget extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('pswidget/widget.phtml');
    }

    protected function _prepareLayout()
    {
        $child = $this->getLayout()->createBlock('pswidget/widget_script');
        $this->setChild(
            'widget_script',
            $child
        );
        return parent::_prepareLayout();
    }

    protected function _beforeToHtml()
    {
        if($child = $this->getChild('widget_script')){
            $child->setId($this->getId())
                ->setPosition($this->getPosition())
                ->setPageType($this->getPageType());
        }
    }

    public function getProductIdJson()
    {
        $product = Mage::registry('product');
        $skus = array();
        if ($product) {
            $skus[] = $product->getSku();
            if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
                $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $product);
                foreach ($childProducts as $child) {
                    $skus[] =  $child->getSku();
                }
            }

            $data['product_id'] = $skus;
        }

        return json_encode($skus);
    }

    public function getProductProductType(){
        $product = Mage::registry('product');
        $categoryIds = $product->getCategoryIds();
        $productTypes = array();
        if($categoryIds){
            $categoryCollection = Mage::getResourceModel('catalog/category_collection')
                ->addAttributeToFilter('entity_id',$categoryIds);
            foreach ($categoryCollection as $category){
                $productTypes[] = Mage::helper('pswidget')->getCategoryPath($category);
            }
        }
        return json_encode($productTypes);
    }

    public function replaceParameter($pattern, $value, $subject){
        $replacement = '$1'.$value;
        return preg_replace($pattern,$replacement,$subject);
    }

    public function getWidgetScript(){
        $widgetScript = $this->getChildHtml('widget_script');
        $patternProductId = '/("productId":)(\[[^]]*])/';
        $patternProductType = '/("productType":)("[^"]*")/';
        if($product = Mage::registry('product')){
            $widgetScript =
                $this->replaceParameter($patternProductId, $this->getProductIdJson(), $widgetScript);
            $widgetScript =
                $this->replaceParameter($patternProductType, $this->getProductProductType(),$widgetScript);
        } elseif ($category = Mage::registry('current_category')){
            $helper = Mage::helper('pswidget');
            $widgetScript =
                $this->replaceParameter($patternProductType,json_encode(array($helper->getCategoryPath($category))),$widgetScript);
        }
        return $widgetScript;
    }
}