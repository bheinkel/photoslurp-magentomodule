<?php

class Photoslurp_PsWidget_Block_Adminhtml_Pswidget_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

        public function __construct()
        {
                parent::__construct();
                $this->setId("pswidgetGrid");
                $this->setDefaultSort("id");
                $this->setDefaultDir("DESC");
                $this->setSaveParametersInSession(true);
        }

        protected function _prepareCollection()
        {
                $collection = Mage::getModel("pswidget/pswidget")->getCollection();
                $this->setCollection($collection);
                return parent::_prepareCollection();
        }
        protected function _prepareColumns()
        {
                $this->addColumn(
                    "id", array(
                    "header" => Mage::helper("pswidget")->__("ID"),
                    "align" =>"right",
                    "width" => "50px",
                    "type" => "number",
                    "index" => "id",
                    )
                );
                        $this->addColumn(
                            'widget_enable', array(
                            'header' => Mage::helper('pswidget')->__('Enable'),
                            'index' => 'widget_enable',
                            'type' => 'options',
                            'options'=>Photoslurp_PsWidget_Block_Adminhtml_Pswidget_Grid::getOptionArrayYesNo(),
                            )
                        );

                $this->addColumn(
                    'page_type', array(
                    'header' => Mage::helper('pswidget')->__('Page Type'),
                    'index' => 'page_type',
                    'type' => 'options',
                    'options'=>Photoslurp_PsWidget_Block_Adminhtml_Pswidget_Grid::getOptionArray10(),
                    )
                );

                return parent::_prepareColumns();
        }

        public function getRowUrl($row)
        {
               return $this->getUrl("*/*/edit", array("id" => $row->getId()));
        }


        
        protected function _prepareMassaction()
        {
            $this->setMassactionIdField('id');
            $this->getMassactionBlock()->setFormFieldName('ids');
            $this->getMassactionBlock()->setUseSelectAll(true);
            $this->getMassactionBlock()->addItem(
                'remove_pswidget', array(
                     'label'=> Mage::helper('pswidget')->__('Remove Pswidget'),
                     'url'  => $this->getUrl('*/adminhtml_pswidget/massRemove'),
                     'confirm' => Mage::helper('pswidget')->__('Are you sure?')
                )
            );
            return $this;
        }
        
        static public function getOptionArray10()
        {
            $dataArray=array();
            $dataArray['']= 'Please select...';
            $dataArray[1]='home';
            $dataArray[2]='product';
            $dataArray[3]='lookbook';
            $dataArray[4]='category';
            return($dataArray);
        }
        static public function getValueArray10()
        {
            $dataArray=array();
            foreach (Photoslurp_PsWidget_Block_Adminhtml_Pswidget_Grid::getOptionArray10() as $k=>$v) {
               $dataArray[]=array('value'=>$k,'label'=>$v);
            }

            return($dataArray);

        }

    static public function getOptionArrayPosition()
    {
        $dataArray=array();
        $dataArray['product-content-bottom']='Product content bottom';
        $dataArray['product-content-top']='Product content top';
        $dataArray['product-media']='Product media';
        $dataArray['product-page-bottom']='Product page bottom';
        $dataArray['product-page-top']='Product page top';

        return($dataArray);
    }
    static public function getValueArrayPosition()
    {
        $dataArray=array();
        foreach (Photoslurp_PsWidget_Block_Adminhtml_Pswidget_Grid::getOptionArrayPosition() as $k=>$v) {
            $dataArray[]=array('value'=>$k,'label'=>$v);
        }

        return($dataArray);

    }
        
        static public function getOptionArray15()
        {
            $dataArray=array();
            $dataArray[1]='facebook ';
            $dataArray[2]='twitter';
            $dataArray[3]='instagram';
            return($dataArray);
        }
        static public function getValueArray15()
        {
            $dataArray=array();
            foreach (Photoslurp_PsWidget_Block_Adminhtml_Pswidget_Grid::getOptionArray15() as $k=>$v) {
               $dataArray[]=array('value'=>$k,'label'=>$v);
            }

            return($dataArray);

        }

        static public function getOptionArrayYesNo()
        {
            $dataArray=array();
            $dataArray[0]='No';
            $dataArray[1]='Yes';
            return($dataArray);
        }
        static public function getValueArrayYesNo()
        {
            $dataArray=array();
            foreach (Photoslurp_PsWidget_Block_Adminhtml_Pswidget_Grid::getOptionArrayYesNo() as $k=>$v) {
                $dataArray[]=array('value'=>$k,'label'=>$v);
            }

            return($dataArray);

        }

        static public function getOptionArrayPhotoOrder()
        {
            $dataArray=array();
            $dataArray[0]='Newest first';
            $dataArray[1]='Random';
            return($dataArray);
        }
        static public function getValueArrayPhotoOrder()
        {
            $dataArray=array();
            foreach (Photoslurp_PsWidget_Block_Adminhtml_Pswidget_Grid::getOptionArrayPhotoOrder() as $k=>$v) {
                $dataArray[]=array('value'=>$k,'label'=>$v);
            }

            return($dataArray);

        }

    static protected $lookBookCategories;

    static public function getValueArrayCategories()
    {
        $categories = Mage::getModel('catalog/category')->getCategories(1);
        self::getCategories($categories);
        return (self::$lookBookCategories);
    }

    static public function getCategories($categories)
    {
        foreach($categories as $category) {
            $labelPrefix = "";
            for($i=0; $i<$category->getLevel(); $i++){
                $labelPrefix .= '..';
            }
            self::$lookBookCategories[] = array('value'=>$category->getId(),'label'=>$labelPrefix.$category->getName());
            if($category->hasChildren()) {
                $children = Mage::getModel('catalog/category')->getCategories($category->getId());
                self::getCategories($children);
            }
        }
    }
}