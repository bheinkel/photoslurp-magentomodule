<?php

class Photoslurp_PsWidget_Block_Adminhtml_Pswidget_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $stores = Mage::app()->getStores();
        $storesCount = count($stores);
        
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            "pswidget_form",
            array("legend" => Mage::helper("pswidget")->__("Configuration parameters"))
        );

        $fieldset->addField(
            'widget_enable', 'select', array(
            'label' => Mage::helper('pswidget')->__('Enable'),
            'values' => Photoslurp_PsWidget_Block_Adminhtml_Pswidget_Grid::getValueArrayYesNo(),
            'name' => 'widget_enable',
            "class" => "required-entry",
            "required" => true,

            )
        );

        $fieldset->addField(
            "user_name", "text", array(
            "label" => Mage::helper("pswidget")->__("User Name"),
            "class" => "required-entry",
            "required" => true,
            "name" => "user_name",
            'note' => 'Your Photoslurp user name'
            )
        );

        $fieldset->addField(
            "widget_id", "text", array(
            "label" => Mage::helper("pswidget")->__("Widget Id"),
            "class" => "required-entry",
            "required" => true,
            "name" => "widget_id"
            )
        );

        $fieldset->addField(
            'page_type', 'select', array(
            'label' => Mage::helper('pswidget')->__('Page Type'),
            'values' => Photoslurp_PsWidget_Block_Adminhtml_Pswidget_Grid::getValueArray10(),
            'name' => 'page_type',
            "class" => "required-entry",
            "required" => true,
            'note' => 'Choose the type of page you want to embed this widget in'
            )
        );

        $websites = array();
        foreach (Mage::app()->getWebsites() as $site){
            $websites[] = array('value'=>$site->getId(), 'label'=>$site->getName());
        }
        $fieldset->addField(
            'website', 'multiselect', array(
                'label' => Mage::helper('pswidget')->__('Website'),
                'values' => $websites,
                'name' => 'website',
                "required" => true,
            )
        );

        $fieldset->addField(
            'display_in_categories', 'select', array(
                'label' => Mage::helper('pswidget')->__('Display In Categories'),
                'values' => array('all'=>'All categories', 'selected'=>'Only in selected categories'),
                'name' => 'display_in_categories',
                'note' => 'Choose whether you want this widget to be displayed in all category pages 
                or in a custom selection of category pages'
            )
        );

        $fieldset->addField(
            'category', 'multiselect', array(
                'label' => Mage::helper('pswidget')->__('Selected Categories'),
                'values' => Photoslurp_PsWidget_Block_Adminhtml_Pswidget_Grid::getValueArrayCategories(),
                'name' => 'category',
                "required" => true,
                "note" => 'To select multiple categories hold down the Ctrl-key on Windows or the Command-key on Mac 
                while clicking on the category names. To select a range: Click the first category, 
                then hold down the Shift-key and then click the last category in the range'
            )
        );

        $fieldset->addField(
            'position', 'select', array(
            'label' => Mage::helper('pswidget')->__('Position'),
            'values' => Photoslurp_PsWidget_Block_Adminhtml_Pswidget_Grid::getValueArrayPosition(),
            'name' => 'position',
            "class" => "required-entry",
            "required" => true,
            'note' => 'Position on product page'
            )
        );

        $fieldset->addField(
            'position_category', 'select', array(
                'label' => Mage::helper('pswidget')->__('Position'),
                'values' => array(
                    array('value' => 'bottom', 'label' => 'Bottom'),
                    array('value' => 'top', 'label' => 'Top')
                ),
                'name' => 'position_category',
                "class" => "required-entry",
                "required" => true,
                'note' => 'Position on category page'
            )
        );

        $fieldset->addField(
            'product_type', 'select', array(
                'label' => Mage::helper('pswidget')->__('Only Show Media From Product Category'),
                'values' => Photoslurp_PsWidget_Block_Adminhtml_Pswidget_Grid::getValueArrayYesNo(),
                'name' => 'product_type',
                'note' => 'When no media is available for a specific product, 
                show only photos related to products in the same product category'
            )
        );

        $fieldset->addField(
            'lookbook_product_types', 'multiselect', array(
                'label' => Mage::helper('pswidget')->__('Only Show Media From Product Categories'),
                'values' => Photoslurp_PsWidget_Block_Adminhtml_Pswidget_Grid::getValueArrayCategories(),
                'name' => 'lookbook_product_types',
                "required" => true,
                'note' => 'Show media related to products in given product categories'
            )
        );

        $htmlIdPrefix = $form->getHtmlIdPrefix();

        $this->setChild(
            'form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
            ->addFieldMap("{$htmlIdPrefix}page_type", 'page_type')
            ->addFieldMap("{$htmlIdPrefix}website", 'website')
            ->addFieldDependence('website', 'page_type', array('2','4'))
            ->addFieldMap("{$htmlIdPrefix}position", 'position')
            ->addFieldDependence('position', 'page_type', '2')
            ->addFieldMap("{$htmlIdPrefix}position_category", 'position_category')
            ->addFieldDependence('position_category', 'page_type', '4')
            ->addFieldMap("{$htmlIdPrefix}category", 'category')
            ->addFieldDependence('category', 'display_in_categories', 'selected')
            ->addFieldDependence('category', 'page_type', '4')
            ->addFieldMap("{$htmlIdPrefix}display_in_categories", 'display_in_categories')
            ->addFieldDependence('display_in_categories', 'page_type', '4')
            ->addFieldMap("{$htmlIdPrefix}product_type", 'product_type')
            ->addFieldDependence('product_type', 'page_type', array('2','3','4'))
            ->addFieldMap("{$htmlIdPrefix}lookbook_product_types", 'lookbook_product_types')
            ->addFieldDependence('lookbook_product_types', 'page_type', '3')
            ->addFieldDependence('lookbook_product_types', 'product_type', '1')
        );

        if (Mage::getSingleton("adminhtml/session")->getPswidgetData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getPswidgetData());
            Mage::getSingleton("adminhtml/session")->setPswidgetData(null);
        } elseif (Mage::registry("pswidget_data")) {
            $values = Mage::registry("pswidget_data")->getData();

            if(isset($values['lookbook_product_types'])){
                $values['lookbook_product_types'] = json_decode($values['lookbook_product_types']);
            }

            if(isset($values['category'])){
                $values['category'] = json_decode($values['category']);
            }

            if(isset($values['website'])){
                $values['website'] = json_decode($values['website']);
            }

            if (!empty($values)) {
                $form->setValues($values);
            }
        }

        return parent::_prepareForm();
    }
}
