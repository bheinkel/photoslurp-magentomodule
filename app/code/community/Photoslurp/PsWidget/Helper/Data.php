<?php
class Photoslurp_PsWidget_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getExportPath()
    {
        $path = Mage::getBaseDir('media');
        $configPath = Mage::getStoreConfig('export/export_configuration/path');

        if ($configPath) {
            $path = Mage::getBaseDir('base') . DS . $configPath;
            $pathInfo = pathinfo($path);
            if (isset($pathInfo['extension'])) {
                $path = $pathInfo['dirname'];
            }
        }

        return $path;
    }

    public function getExportFile()
    {
        $file = 'photoslurp_export.csv';
        $configPath = Mage::getStoreConfig('export/export_configuration/path');
        if ($configPath) {
            $pathInfo = pathinfo($configPath);
            if (isset($pathInfo['extension'])) {
                $file = $pathInfo['basename'];
            }
        }

        return $file;
    }

    public function getFileUrl()
    {
        $path = $this->getExportPath() . DS . $this->getExportFile();
        $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)
            .str_replace(Mage::getBaseDir('base').DS, '', $path);
        return $url;

    }

    public function fileExists()
    {
        $io = new Varien_Io_File();
        return $io->fileExists($this->getExportPath() . DS . $this->getExportFile());
    }

    public function getExtensionVersion()
    {
        return (string) Mage::getConfig()->getNode()->modules->Photoslurp_PsWidget->version;
    }

    public function getCategoryPath($category){
        $pathArray = explode('/',$category->getPath());
        unset($pathArray[0]);
        $pathCategories = Mage::getResourceModel('catalog/category_collection')->addNameToResult()
            ->addAttributeToFilter('entity_id',$pathArray);
        $names = array();
        foreach ($pathCategories as $pathCategory){
            $names[] = $pathCategory->getName();
        }
        return implode('>',$names);
    }
}
     