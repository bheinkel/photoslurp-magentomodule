<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'note_add_pics_text');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidgetlang'), 'note_add_pics_text');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'note_add_pics_icons');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'image_height');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'image_width');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'bundled_jquery');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'social_count');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'one_photo_per_line');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'fix_widget_analytics');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'submission_form_url');
$installer->getConnection()->addColumn($installer->getTable('pswidget/pswidget'), 'theme', "varchar(255) NULL");

$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_popup_productCaptionShop_font_family'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_popup_productCaptionShop_font_style'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_popup_productCaptionShop_font_weight'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_popup_productCaptionShop_font_color'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_popup_productDescription_font_family'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
     'style_popup_productDescription_font_style'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
     'style_popup_productDescription_font_weight'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
     'style_popup_productDescription_font_color'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
     'style_taggingTitle_font_family'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
     'style_taggingTitle_font_style'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_taggingTitle_font_weight'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
     'style_taggingTitle_font_color'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
     'style_taggingTitle_font_size'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
     'style_submissionForm_colourTop'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
     'style_submissionForm_colourButton'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
     'style_submissionForm_font'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
     'style_popup_source_font_family'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
     'style_popup_source_font_style'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
     'style_popup_source_font_weight'
);
$installer->getConnection()->dropColumn(
    $installer->getTable('pswidget/pswidget'),
     'style_popup_source_font_color'
);

$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'),'style_thumbnail_bg_color');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'),'style_thumbnail_border_color');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'),'style_carousel_bg_color');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'),'style_popup_bg_color');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'),'style_popup_title_font_family');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'),'style_popup_title_font_style');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'),'style_popup_title_font_weight');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'),'style_popup_title_font_color');

$installer->endSetup();