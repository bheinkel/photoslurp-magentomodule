<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('pswidget/pswidget'), 'category', "TEXT DEFAULT NULL");
$installer->getConnection()->addColumn($installer->getTable('pswidget/pswidget'), 'display_in_categories', "varchar(255) NULL");

$installer->endSetup();