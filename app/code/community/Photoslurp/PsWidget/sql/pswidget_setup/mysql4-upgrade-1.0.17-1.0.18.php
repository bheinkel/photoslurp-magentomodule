<?php
$installer = $this;
$connection = $installer->getConnection();

$installer->startSetup();
$installer->getConnection()
    ->addColumn($installer->getTable('pswidget/pswidget'), 'visible_products', "varchar(255) NULL");

if ($installer->tableExists($table = $installer->getTable('permission_block'))) {
    $installer->getConnection()->insertMultiple(
        $table,
        array(
            array('block_name' => 'pswidget/widget', 'is_allowed' => 1),
        )
    );
}

$installer->endSetup();