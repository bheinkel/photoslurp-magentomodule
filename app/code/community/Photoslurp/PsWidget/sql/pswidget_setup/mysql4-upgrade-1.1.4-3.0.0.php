<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'photos_align');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'cross_domain_tracking');

$installer->endSetup();