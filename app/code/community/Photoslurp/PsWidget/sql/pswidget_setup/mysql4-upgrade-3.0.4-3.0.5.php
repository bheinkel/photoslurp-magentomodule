<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('pswidget/pswidget'), 'product_type', "tinyint(1) DEFAULT NULL");
$installer->getConnection()->addColumn($installer->getTable('pswidget/pswidget'), 'lookbook_product_types', "TEXT DEFAULT NULL");
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'allow_empty');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'init_delay');

$installer->endSetup();