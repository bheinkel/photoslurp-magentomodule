<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('pswidget/pswidget'), 'position_category', "varchar(255) NULL");

$installer->endSetup();