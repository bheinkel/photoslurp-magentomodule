<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'collections', 'collection', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'analytics_cookie_ttl', 'analytics_cookie_TTL', 'int'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'submission_form_css_url', 'submission_form_CSS_URL', 'text'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_productcaptionshop_font_family', 'style_popup_productCaptionShop_font_family', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_productcaptionshop_font_style', 'style_popup_productCaptionShop_font_style', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_productcaptionshop_font_weight', 'style_popup_productCaptionShop_font_weight', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_productcaptionshop_font_color', 'style_popup_productCaptionShop_font_color', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_productdescription_font_family', 'style_popup_productDescription_font_family', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_productdescription_font_style', 'style_popup_productDescription_font_style', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_productdescription_font_weight', 'style_popup_productDescription_font_weight', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_productdescription_font_color', 'style_popup_productDescription_font_color', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_taggingtitle_font_family', 'style_taggingTitle_font_family', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_taggingtitle_font_style', 'style_taggingTitle_font_style', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_taggingtitle_font_weight', 'style_taggingTitle_font_weight', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_taggingtitle_font_color', 'style_taggingTitle_font_color', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_taggingtitle_font_size', 'style_taggingTitle_font_size', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_submissionform_colourtop', 'style_submissionForm_colourTop', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_submissionform_colourbutton', 'style_submissionForm_colourButton', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_submissionform_font', 'style_submissionForm_font', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_source_font_family', 'style_popup_source_font_family', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_source_font_style', 'style_popup_source_font_style', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_source_font_weight', 'style_popup_source_font_weight', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_source_font_color', 'style_popup_source_font_color', 'varchar(255)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'enable_ga', 'enable_g_a', 'tinyint(1)'
);
$installer->getConnection()->changeColumn(
    $installer->getTable('pswidget/pswidget'),
    'style_custom', 'css', 'longtext'
);

$installer->getConnection()->addColumn($installer->getTable('pswidget/pswidget'), 'photos_align', "varchar(255) NULL");

$installer->getConnection()
    ->addColumn($installer->getTable('pswidget/pswidget'), 'utm_params', "tinyint(1) DEFAULT NULL");
$installer->getConnection()->addColumn($installer->getTable('pswidget/pswidget'), 'utm_source', "varchar(255) NULL");
$installer->getConnection()->addColumn($installer->getTable('pswidget/pswidget'), 'utm_medium', "varchar(255) NULL");
$installer->getConnection()->addColumn($installer->getTable('pswidget/pswidget'), 'utm_campaign', "varchar(255) NULL");
$installer->getConnection()->addColumn($installer->getTable('pswidget/pswidget'), 'utm_content', "varchar(255) NULL");

$installer->getConnection()->addColumn(
    $installer->getTable('pswidget/pswidgetlang'), 'posted_by_text',
    "text NULL"
);
$installer->getConnection()->addColumn(
    $installer->getTable('pswidget/pswidgetlang'), 'view_and_shop_text',
    "text NULL"
);

$installer->getConnection()
    ->addColumn($installer->getTable('pswidget/pswidget'), 'same_tab_links', "tinyint(1) DEFAULT NULL");

$installer->getConnection()
    ->addColumn($installer->getTable('pswidget/pswidget'), 'cross_domain_tracking', "tinyint(1) DEFAULT NULL");

$installer->endSetup();