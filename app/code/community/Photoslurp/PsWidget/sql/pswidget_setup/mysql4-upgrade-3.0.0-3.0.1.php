<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('pswidget/pswidget'), 'website', "varchar(255) NULL");

$installer->endSetup();