<?php
$installer = $this;
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('pswidget/pswidgetlang'))
    ->addColumn(
        'id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Id'
    )
    ->addColumn(
        'id_widget', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Id Widget'
    )
    ->addColumn(
        'id_lang', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Id Lang'
    )
    ->addColumn('submit_text', Varien_Db_Ddl_Table::TYPE_TEXT, 255)
    ->addColumn('note_add_pics_text', Varien_Db_Ddl_Table::TYPE_TEXT, 255);

$installer->getConnection()->createTable($table);

$installer->endSetup();
