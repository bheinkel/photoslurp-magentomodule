<?php
$installer = $this;
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('pswidget/pswidget'))
    ->addColumn(
        'id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Id'
    )
    ->addColumn('widget_enable', Varien_Db_Ddl_Table::TYPE_SMALLINT)
    ->addColumn('widget_container_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('widget_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('widget_type', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('album_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('lang', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('page_limit', Varien_Db_Ddl_Table::TYPE_SMALLINT)
    ->addColumn('page_type', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('show_submit', Varien_Db_Ddl_Table::TYPE_SMALLINT)
    ->addColumn('submit_text', Varien_Db_Ddl_Table::TYPE_TEXT, 255)
    ->addColumn('add_photos_img', Varien_Db_Ddl_Table::TYPE_TEXT, 255)
    ->addColumn('note_add_pics_text', Varien_Db_Ddl_Table::TYPE_TEXT, 255)
    ->addColumn('note_add_pics_icons', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('social_icons', Varien_Db_Ddl_Table::TYPE_SMALLINT)
    ->addColumn('image_height', Varien_Db_Ddl_Table::TYPE_SMALLINT)
    ->addColumn('image_width', Varien_Db_Ddl_Table::TYPE_SMALLINT)
    ->addColumn('style_submissionform_colourtop', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_submissionform_colourbutton', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_submissionform_font', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_taggingtitle_font_family', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_taggingtitle_font_style', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_taggingtitle_font_weight', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_taggingtitle_font_color', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_taggingtitle_font_size', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_thumbnail_bg_color', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_thumbnail_border_color', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_carousel_bg_color', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_popup_bg_color', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_popup_title_font_family', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_popup_title_font_style', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_popup_title_font_weight', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_popup_title_font_color', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_source_font_family', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_source_font_style', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_source_font_weight', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_source_font_color', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_productcaptionshop_font_family', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_productcaptionshop_font_style', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_productcaptionshop_font_weight', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_productcaptionshop_font_color', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_productdescription_font_family', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_productdescription_font_style', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_productdescription_font_weight', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_productdescription_font_color', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
    ->addColumn('style_custom', Varien_Db_Ddl_Table::TYPE_TEXT)
    ->addColumn('style_custom_enable', Varien_Db_Ddl_Table::TYPE_SMALLINT);

$installer->getConnection()->createTable($table);

$installer->endSetup();