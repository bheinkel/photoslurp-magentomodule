<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'page_limit');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'album_id');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'show_submit');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'add_photos_img');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'social_icons');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'random_order');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'autoscroll_limit');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'enable_g_a');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'submission_form_CSS_URL');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'thumb_overlay');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'varying_thumb_sizes');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'auto_scroll_carousel');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'analytics_cookie_TTL');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'lightbox');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'toc_link');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'strict_products');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'empty_threshold');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'in_stock_only');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'rights_cleared_only');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'assigned_only');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'visible_products');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'collection');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'cookie_domain');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'utm_params');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'utm_source');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'utm_medium');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'utm_campaign');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'utm_content');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'same_tab_links');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'theme');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'additional_params');
$installer->getConnection()->dropColumn($installer->getTable('pswidget/pswidget'), 'widget_type');

$installer->getConnection()->dropTable($installer->getTable('pswidget/pswidgetlang'));

$installer->endSetup();